import os
import pickle
from itertools import combinations

import numpy as np
from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QFileDialog, QLabel, QSpinBox
from PySide2.QtWidgets import QGridLayout

from sknrf.settings import Settings
from sknrf.device.signal import tf
from sknrf.device.instrument import rfztuner as calkit_rfztuner
from sknrf.device.instrument.rfreceiver import NoRFReceiver
from sknrf.device.instrument.rfsource import NoRFSource
from sknrf.model.calibration.base import calkit_connector_map, instrument_enable_map
from sknrf_app_calibration_vector.model.vector import OnePortModel, SDDLModel, PHNModel
from sknrf_app_calibration_vector.model.vector import TwelveTermModel, SOLTModel, EightTermModel, UnknownThruModel, TRLModel
from sknrf_app_calibration_vector.model.vector import TwoPortOnePathModel
from sknrf.app.dataviewer.model.snp import SNP
from sknrf.enums.device import Instrument
from sknrf.view.desktop.calibration.wizard.base import AbstractCalibrationWizard
from sknrf.view.desktop.calibration.wizard.base import AbstractContentPage, AbstractConclusionPage
from sknrf.view.desktop.calibration.wizard.base import AbstractPortPage, AbstractInstrumentPage, \
    AbstractRequirementsPage
from sknrf.utilities.patterns import export


class VectorPortPage(AbstractPortPage):
    pass


class VectorInstrumentPage(AbstractInstrumentPage):

    def __init__(self, _parent):
        super(VectorInstrumentPage, self).__init__(_parent)
        checked_map = instrument_enable_map.copy()
        checked_map[Instrument.RFSOURCE] = False
        self.add_instrument(self.wizard().model().instrument_flags, checked_map=checked_map)


class VectorRequirementsPage(AbstractRequirementsPage):

    def __init__(self, _parent):
        super(VectorRequirementsPage, self).__init__(_parent)
        self.add_requirement("LF Source is OFF for all ports.")
        self.add_requirement("RF Source is NOT connected for calibration port")
        self.add_requirement("RF Source is connected for measurement ports.")
        self.add_requirement("RF Source is OFF for all ports.")
        self.add_requirement("RF Source.a_p > min for measurement ports.")
        self.add_requirement("RF Receiver is connected for measurement ports.")

        self.add_recommendation("RF ZTuner.z_set == 50 Ohm for all ports.")
        self.connect_signals()

    def check_requirements(self):
        super(VectorRequirementsPage, self).check_requirements()
        state = [True]*len(self.requirementsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                state[1] = state[1] and type(port.rfsource) is NoRFSource
            else: # Measurement Port
                state[2] = state[2] and type(port.rfsource) is not NoRFSource
                info = port.rfsource.info
                min, abs_tol = info["a_p"].min, info["a_p"].abs_tol
                state[4] = state[4] and np.all(np.abs(tf.pk(port.rfsource.a_p)) > min + abs_tol)
                state[5] = state[5] and type(port.rfreceiver) is not NoRFReceiver
            # All Ports
            port.lfsource.on = False
            port.rfsource.on = False
            state[0] = state[0] and not port.lfsource.on
            state[3] = state[3] and not port.rfsource.on
        for index, value in enumerate(state):
            self.requirementsCheckBoxList[index].setChecked(value)

    def check_recommendations(self):
        super(VectorRequirementsPage, self).check_recommendations()
        state = [True] * len(self.recommendationsCheckBoxList)
        port_indices = [0] + self.wizard().model().port_indices
        for port_index in port_indices:
            port = self.wizard().model().device_model().ports[port_index]
            if port_index == 0: # Calibration Port
                pass
            else: # Measurement Port
                pass
            # All Ports
            info = port.rfztuner.info
            rel_tol, abs_tol = info["z_set"].rel_tol, info["z_set"].abs_tol
            state[0] = state[0] and np.allclose(tf.avg(port.rfztuner.z_set), 50.0, rel_tol, abs_tol)
        for index, value in enumerate(state):
            self.recommendationsCheckBoxList[index].setChecked(value)


class VectorContentPage(AbstractContentPage):

        def isComplete(self):
            """ Defines the conditions that must be satisfied before proceeding to the the next page.
            """
            ideal_ntwks = self.wizard().model().ideal_ntwks
            measured_ntwks = self.wizard().model().measured_ntwks
            return self.optional or \
                   (len(self.name) and self.name[0] != '_' and self.name in ideal_ntwks and self.name in measured_ntwks)


class VectorConclusionPage(AbstractConclusionPage):
    pass


class VectorCalibrationWizard(AbstractCalibrationWizard):

    _port_nums = [1, 2]
    _instruments = [Instrument.RF, Instrument.RF]

    def __init__(self, parent=None, calkit_package=None, calkit_icon=QIcon()):
        super(VectorCalibrationWizard, self).__init__(parent=parent, calkit_package=calkit_package, calkit_icon=calkit_icon)
        self.measurement_type = "SS"

    def save(self):
        filenames = QFileDialog.getSaveFileName(self,
                                                "Save Calibration",
                                                os.sep.join((Settings().data_root, "saved_calibrations")),
                                                "Calibration File (*.cal);;S-Parameter File (*.s2p)")

        if len(filenames[0]):
            filename = filenames[0]
            file_name, extension = os.path.splitext(filename)
            if extension == ".cal":
                with open(filename, "wb") as file_id:
                    pickle.dump(self._model.calibration, file_id)
                    return True
            elif extension == ".s2p":
                for port_index in self.port_indices():
                    SNP.write_network(self._model.calibration.error_ntwk[port_index],
                                      "%s%d%s" %(file_name, port_index + 1, extension))
                return True
        return False


class OnePortConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class OnePortInstrumentPage(VectorInstrumentPage):
    pass


class OnePortIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(OnePortIntroductionPage, self).__init__(_parent)
        self.setTitle("One Port Calibration")
        self.setSubTitle("Generic One-Port Calibration")


class OnePortContentPage(VectorContentPage):
    pass


class OnePortConclusionPage(VectorConclusionPage):
    pass


class OnePortWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(OnePortWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = OnePortModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(OnePortConnectionPage(self))
        self.addPage(OnePortInstrumentPage(self))
        self.addPage(OnePortIntroductionPage(self))
        self.addPage(OnePortConclusionPage(self))

    def initialize_content_pages(self):
        super(OnePortWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" %(port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "__Reflection"]
            pages.append(OnePortContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(OnePortContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(OnePortContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(OnePortContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        page_id = self.currentId()
        _, port_nums, _, _ = self.currentPage().name_info(0)
        port_indices = np.array(list(filter(None, port_nums.split("_")))).astype(np.int) - 1
        port_num = port_indices[0] + 1
        port_id = "Port%d" % (port_num,)
        pages = [OnePortContentPage(self, "__Reflection_%d" % (port_num,), [port_id, "Adapter", "__Reflection"], optional=True)]
        self.insert_content_pages(pages, page_id=page_id)


class SDDLConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class SDDLInstrumentPage(VectorInstrumentPage):
    pass


class SDDLIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(SDDLIntroductionPage, self).__init__(_parent)
        self.setTitle("One Port Calibration")
        self.setSubTitle("SDDL Calibration")


class SDDLContentPage(VectorContentPage):
    pass


class SDDLConclusionPage(VectorConclusionPage):
    pass


class SDDLWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(SDDLWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = SDDLModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(SDDLConnectionPage(self))
        self.addPage(SDDLInstrumentPage(self))
        self.addPage(SDDLIntroductionPage(self))
        self.addPage(SDDLConclusionPage(self))

    def initialize_content_pages(self):
        super(SDDLWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            calkit_filepath = calkit_connector_map[calkit_connector]
            contents = [port_id, "Short"] if port_connector == calkit_connector else [port_id, "Adapter", "Short"]
            pages.append(SDDLContentPage(self, "Short_%d" % (port_num,), contents, optional=False))
            self.set_ideal(contents.index("Short"), os.sep.join((calkit_filepath, "short.s1p")), pages[-1])
            contents = [port_id, "Delay"] if port_connector == calkit_connector else [port_id, "Adapter", "Delay"]
            pages.append(SDDLContentPage(self, "Delay1_%d" % (port_num,), contents, optional=False))
            self._model.ideal_ntwks["Delay1_%d" % (port_num,)] = None
            contents = [port_id, "Delay"] if port_connector == calkit_connector else [port_id, "Adapter", "Delay"]
            pages.append(SDDLContentPage(self, "Delay2_%d" % (port_num,), contents, optional=False))
            self._model.ideal_ntwks["Delay2_%d" % (port_num,)] = None
            contents = [port_id, "Load"] if port_connector == calkit_connector else [port_id, "Adapter", "Load"]
            pages.append(SDDLContentPage(self, "Load_%d" % (port_num,), contents, optional=False))
            self.set_ideal(contents.index("Load"), os.sep.join((calkit_filepath, "load.s1p")), pages[-1])
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")


class PHNConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class PHNInstrumentPage(VectorInstrumentPage):
    pass


class PHNIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(PHNIntroductionPage, self).__init__(_parent)
        self.setTitle("One Port Calibration")
        self.setSubTitle("PHN Calibration")


class PHNContentPage(VectorContentPage):
    pass


class PHNConclusionPage(VectorConclusionPage):
    pass


class PHNWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(PHNWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = PHNModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(PHNConnectionPage(self))
        self.addPage(PHNInstrumentPage(self))
        self.addPage(PHNIntroductionPage(self))
        self.addPage(PHNConclusionPage(self))

    def initialize_content_pages(self):
        super(PHNWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__HalfKnownReflection"] if port_connector == calkit_connector \
                else [port_id, "Adapter", "__HalfKnownReflection"]
            pages.append(PHNContentPage(self, "__HalfKnownReflection_%d" % (port_num,), contents, optional=False))
            pages.append(PHNContentPage(self, "__HalfKnownReflection_%d" % (port_num,), contents, optional=False))
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector \
                else [port_id, "Adapter", "__Reflection"]
            pages.append(PHNContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(PHNContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")


class TwelveTermConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) > 1


class TwelveTermInstrumentPage(VectorInstrumentPage):
    pass


class TwelveTermIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(TwelveTermIntroductionPage, self).__init__(_parent)
        self.setTitle("Two Port Calibration")
        self.setSubTitle("Generic Twelve-Term Calibration")


class TwelveTermContentPage(VectorContentPage):
    pass


class TwelveTermConclusionPage(VectorConclusionPage):
    pass


class TwelveTermWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(TwelveTermWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = TwelveTermModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(TwelveTermConnectionPage(self))
        self.addPage(TwelveTermInstrumentPage(self))
        self.addPage(TwelveTermIntroductionPage(self))
        self.addPage(TwelveTermConclusionPage(self))

    def initialize_content_pages(self):
        super(TwelveTermWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" %(port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "__Reflection"]
            pages.append(TwelveTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(TwelveTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(TwelveTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(TwelveTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True))
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["__Transmission"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(TwelveTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=False))
            pages.append(TwelveTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        page_id = self.currentId()
        _, port_nums, _, _ = self.currentPage().name_info(0)
        port_indices = np.array(list(filter(None, port_nums.split("_")))).astype(np.int) - 1
        if len(port_indices) > 1:
            combo = port_indices
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["__Transmission"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages = [TwelveTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True)]
        else:
            port_index = port_indices[0]
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector \
                else [port_id, "Adapter", "__Reflection"]
            pages = [TwelveTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True)]
        self.insert_content_pages(pages, page_id=page_id)


class SOLTConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) > 1


class SOLTInstrumentPage(VectorInstrumentPage):
    pass


class SOLTIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(SOLTIntroductionPage, self).__init__(_parent)
        self.setTitle("Two Port Calibration")
        self.setSubTitle("SOLT Calibration")


class SOLTContentPage(VectorContentPage):
    pass


class SOLTConclusionPage(VectorConclusionPage):
    pass


class SOLTWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(SOLTWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = SOLTModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(SOLTConnectionPage(self))
        self.addPage(SOLTInstrumentPage(self))
        self.addPage(SOLTIntroductionPage(self))
        self.addPage(SOLTConclusionPage(self))

    def initialize_content_pages(self):
        super(SOLTWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            calkit_filepath = calkit_connector_map[calkit_connector]
            contents = [port_id, "Short"] if port_connector == calkit_connector else [port_id, "Adapter", "Short"]
            pages.append(SOLTContentPage(self, "Short_%d" % (port_num,), contents, optional=False))
            self.set_ideal(contents.index("Short"), os.sep.join((calkit_filepath, "short.s1p")), pages[-1])
            contents = [port_id, "Open"] if port_connector == calkit_connector else [port_id, "Adapter", "Open"]
            pages.append(SOLTContentPage(self, "Open_%d" % (port_num,), contents, optional=False))
            self.set_ideal(contents.index("Open"), os.sep.join((calkit_filepath, "open.s1p")), pages[-1])
            contents = [port_id, "Load"] if port_connector == calkit_connector else [port_id, "Adapter", "Load"]
            pages.append(SOLTContentPage(self, "Load_%d" % (port_num,), contents, optional=False))
            self.set_ideal(contents.index("Load"), os.sep.join((calkit_filepath, "load.s1p")), pages[-1])
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            ext = os.path.split(calkit_connector_map[calkit_connector2])[1]
            calkit_filepath = calkit_connector_map[calkit_connector1] + "_" + ext
            contents = [port_id1] + ["Adapter"]*int(port_connector1 != calkit_connector1) + \
                       ["Thru"] + \
                       ["Adapter"]*int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(SOLTContentPage(self, "Thru_%d_%d" % (port_num1, port_num2), contents, optional=False))
            self.set_ideal(contents.index("Thru"), os.sep.join((calkit_filepath, "thru.s2p")), pages[-1])
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")


class EightTermConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) > 1


class EightTermInstrumentPage(VectorInstrumentPage):
    pass


class EightTermIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(EightTermIntroductionPage, self).__init__(_parent)
        self.setTitle("Two Port Calibration")
        self.setSubTitle("Generic Eight-Term Calibration")


class EightTermContentPage(VectorContentPage):
    pass


class EightTermConclusionPage(VectorConclusionPage):
    pass


class EightTermWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(EightTermWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = EightTermModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(EightTermConnectionPage(self))
        self.addPage(EightTermInstrumentPage(self))
        self.addPage(EightTermIntroductionPage(self))
        self.addPage(EightTermConclusionPage(self))

    def initialize_content_pages(self):
        super(EightTermWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" %(port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "__Reflection"]
            pages.append(EightTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(EightTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(EightTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(EightTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True))
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["__Transmission"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(EightTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=False))
            pages.append(EightTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        page_id = self.currentId()
        _, port_nums, _, _ = self.currentPage().name_info(0)
        port_indices = np.array(list(filter(None, port_nums.split("_")))).astype(np.int) - 1
        if len(port_indices) > 1:
            combo = port_indices
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["__Transmission"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages = [EightTermContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True)]
        else:
            port_index = port_indices[0]
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector \
                else [port_id, "Adapter", "__Reflection"]
            pages = [EightTermContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True)]
        self.insert_content_pages(pages, page_id=page_id)


class UnknownThruConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) > 1


class UnknownThruInstrumentPage(VectorInstrumentPage):
    pass


class UnknownThruIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(UnknownThruIntroductionPage, self).__init__(_parent)
        self.setTitle("Two Port Calibration")
        self.setSubTitle("Unknown-Thru Calibration")


class UnknownThruContentPage(VectorContentPage):
    pass


class UnknownThruConclusionPage(VectorConclusionPage):
    pass


class UnknownThruWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(UnknownThruWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = UnknownThruModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(UnknownThruConnectionPage(self))
        self.addPage(UnknownThruInstrumentPage(self))
        self.addPage(UnknownThruIntroductionPage(self))
        self.addPage(UnknownThruConclusionPage(self))

    def initialize_content_pages(self):
        super(UnknownThruWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" %(port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector\
                else [port_id, "Adapter", "__Reflection"]
            pages.append(UnknownThruContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(UnknownThruContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(UnknownThruContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=False))
            pages.append(UnknownThruContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True))
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            ext = os.path.split(calkit_connector_map[calkit_connector2])[1]
            calkit_filepath = calkit_connector_map[calkit_connector1] + "_" + ext
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["Thru"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(UnknownThruContentPage(self, "Thru_%d_%d" % (port_num1, port_num2), contents, optional=False))
            self.set_ideal(contents.index("Thru"), os.sep.join((calkit_filepath, "thru.s2p")), pages[-1])
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        page_id = self.currentId()
        _, port_nums, _, _ = self.currentPage().name_info(0)
        port_indices = np.array(list(filter(None, port_nums.split("_")))).astype(np.int) - 1
        if len(port_indices) > 1:
            raise NotImplementedError("Optional Transmission content pages are not allowed for this calibration.")
        else:
            port_index = port_indices[0]
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "__Reflection"] if port_connector == calkit_connector \
                else [port_id, "Adapter", "__Reflection"]
            pages = [UnknownThruContentPage(self, "__Reflection_%d" % (port_num,), contents, optional=True)]
        self.insert_content_pages(pages, page_id=page_id)


class TRLConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) > 1


class TRLInstrumentPage(VectorInstrumentPage):
    pass


class TRLIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(TRLIntroductionPage, self).__init__(_parent)
        self.setTitle("Two Port Calibration")
        self.setSubTitle("TRL Calibration")


class TRLContentPage(VectorContentPage):
    pass


class TRLConclusionPage(VectorConclusionPage):
    pass



class TRLWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(TRLWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        if model is None:
            model = TRLModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(TRLConnectionPage(self))
        self.addPage(TRLInstrumentPage(self))
        self.addPage(TRLIntroductionPage(self))
        self.addPage(TRLConclusionPage(self))

    def initialize_content_pages(self):
        super(TRLWizard, self).initialize_content_pages()
        pages = list()
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()
        for port_index in self.port_indices():
            port_num = port_index + 1
            port_id = "Port%d" % (port_num,)
            port_connector = self._model.port_connectors[port_index]
            calkit_connector = self._model.calkit_connectors[port_index]
            contents = [port_id, "Reflect"] if port_connector == calkit_connector else [port_id, "Adapter", "Reflect"]
            pages.append(TRLContentPage(self, "Reflect_%d" % (port_num,), contents, optional=False))
            self._model.ideal_ntwks["Reflect_%d" % (port_num,)] = None
        for combo in combinations(self.port_indices(), 2):
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"]*int(port_connector1 != calkit_connector1) + \
                       ["Thru"] + \
                       ["Adapter"]*int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(TRLContentPage(self, "Thru_%d_%d" % (port_num1, port_num2), contents, optional=False))
            self._model.ideal_ntwks["Thru_%d_%d" % (port_num1, port_num2)] = None
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["Line"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(TRLContentPage(self, "Line_%d_%d" % (port_num1, port_num2), contents, optional=False))
            self._model.ideal_ntwks["Line_%d_%d" % (port_num1, port_num2)] = None
            pages.append(TRLContentPage(self, "Line_%d_%d" % (port_num1, port_num2), contents, optional=True))
            self._model.ideal_ntwks["Line_%d_%d" % (port_num1, port_num2)] = None
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        raise NotImplementedError("Optional content pages are not allowed for this calibration.")


class TwoPortOnePathConnectionPage(VectorPortPage):

    def isComplete(self):
        return len(self.wizard().port_indices()) == 1


class TwoPortOnePathInstrumentPage(VectorInstrumentPage):
    pass


class TwoPortOnePathIntroductionPage(VectorRequirementsPage):

    def __init__(self, _parent):
        super(TwoPortOnePathIntroductionPage, self).__init__(_parent)
        self.setTitle("Thru Calibration")
        self.setSubTitle("Thru Calibration")

        self.sourcePortGBL = QGridLayout(self.requirementsFrame)
        self.requirementsLabel = QLabel("Calibration Requirements:", parent=self.requirementsFrame)
        self.requirementsGBL.addWidget(self.requirementsLabel, 0, 0, 1, 1)
        self.requirementsCheckBoxList = list()
        self.vbl.addWidget(self.requirementsFrame)

        self.sourcePortLabel = QLabel("Source Port:",  parent=self)
        self.sourcePortSpinBox = QSpinBox(parent=self)
        self.sourcePortSpinBox.setMinimum(0)
        self.sourcePortSpinBox.setMaximum(Settings().num_ports)
        self.requirementsGBL.addWidget(self.sourcePortLabel, self.requirementsGBL.rowCount(), 0, 1, 1)
        self.requirementsGBL.addWidget(self.sourcePortSpinBox, self.requirementsGBL.rowCount()-1, 1, 1, 1)
        self.add_requirement("LF Source is turned off.")
        self.add_requirement("RF Source is connected to each port.")

        self.add_recommendation("RF ZTuner should be matched to the Load impedance.")

    def connect_signals(self):
        pass
        # super(TwoPortOnePathIntroductionPage, self).connect_signals()
        # self.sourcePortSpinBox.valueChanged.connect(self.update)

    def disconnect_signals(self):
        pass
        # super(TwoPortOnePathIntroductionPage, self).connect_signals()
        # self.sourcePortSpinBox.valueChanged.disconnect(self.update)

    def check_requirements(self):
        super(TwoPortOnePathIntroductionPage, self).check_requirements()
        for port_index in self.wizard().port_indices():
            port = self.wizard().model().device_model().ports[port_index]
            port.lfsource.on = False
            self.requirementsCheckBoxList[0].setChecked(not port.lfsource.on)
            if not isinstance(port.rfsource, NoRFSource):
                self.requirementsCheckBoxList[1].setChecked(True)
            else:
                self.requirementsCheckBoxList[1].setChecked(False)

    def check_recommendations(self):
        super(TwoPortOnePathIntroductionPage, self).check_recommendations()
        for port_index in self.wizard().port_indices():
            port = self.wizard().model().device_model().ports[port_index]
            if np.all(np.equal(port.rfztuner.z, 50)):
                self.recommendationsCheckBoxList[0].setChecked(True)
            else:
                self.recommendationsCheckBoxList[0].setChecked(False)

    def update(self, state=False):
        all_ = not state
        super(AbstractRequirementsPage, self).update()

        if self.wizard() and state or all_:
            self.wizard().source_port_index = self.sourcePortSpinBox.value() - 1
            self.wizard().initialize_content_pages()

class TwoPortOnePathContentPage(VectorContentPage):
    pass


class TwoPortOnePathConclusionPage(VectorConclusionPage):
    pass


class TwoPortOnePathWizard(VectorCalibrationWizard):

    @export
    def __init__(self, parent=None, model=None):
        calkit_icon = QIcon(":/PNG/black/64/rfztuner_ref.png")
        super(TwoPortOnePathWizard, self).__init__(parent=parent, calkit_package=calkit_rfztuner, calkit_icon=calkit_icon)
        self.source_port_index = -1
        if model is None:
            model = TwoPortOnePathModel()
        calkit_model = model.device_model().ports[0].rfztuner
        self.set_model(model, calkit_model)

        self.addPage(TwoPortOnePathConnectionPage(self))
        self.addPage(TwoPortOnePathInstrumentPage(self))
        self.addPage(TwoPortOnePathIntroductionPage(self))
        self.addPage(TwoPortOnePathConclusionPage(self))

    def initialize_content_pages(self):
        super(TwoPortOnePathWizard, self).initialize_content_pages()
        pages = list()
        self._model.source_port_index = self.source_port_index + 1
        self._model.ideal_ntwks.clear()
        self._model.measured_ntwks.clear()

        for port_index in self.port_indices():
            combo = [self.source_port_index, port_index]
            port_num1, port_num2 = combo[0] + 1, combo[1] + 1
            port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
            port_connector1 = self._model.port_connectors[combo[0]]
            port_connector2 = self._model.port_connectors[combo[1]]
            calkit_connector1 = self._model.calkit_connectors[combo[0]]
            calkit_connector2 = self._model.calkit_connectors[combo[1]]
            contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                       ["__Transmission"] + \
                       ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
            pages.append(
                TwoPortOnePathContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=False))
            pages.append(
                TwoPortOnePathContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True))
        self.insert_content_pages(pages)

    def insert_optional_content_page(self):
        page_id = self.currentId()
        _, port_nums, _, _ = self.currentPage().name_info(0)
        port_indices = np.array(list(filter(None, port_nums.split("_")))).astype(np.int) - 1
        combo = port_indices
        port_num1, port_num2 = combo[0] + 1, combo[1] + 1
        port_id1, port_id2 = "Port%d" % (port_num1,), "Port%d " % (port_num2,)
        port_connector1 = self._model.port_connectors[combo[0]]
        port_connector2 = self._model.port_connectors[combo[1]]
        calkit_connector1 = self._model.calkit_connectors[combo[0]]
        calkit_connector2 = self._model.calkit_connectors[combo[1]]
        contents = [port_id1] + ["Adapter"] * int(port_connector1 != calkit_connector1) + \
                   ["__Transmission"] + \
                   ["Adapter"] * int(port_connector2 != calkit_connector2) + [port_id2]
        pages = [TwoPortOnePathContentPage(self, "__Transmission_%d_%d" % (port_num1, port_num2), contents, optional=True)]
        self.insert_content_pages(pages, page_id=page_id)


class EnhancedResponseConnectionPage(TwoPortOnePathConnectionPage):
    pass


class EnhancedResponseInstrumentPage(TwoPortOnePathInstrumentPage):
    pass


class EnhancedResponseIntroductionPage(TwoPortOnePathIntroductionPage):
    pass


class EnhancedResponseContentPage(TwoPortOnePathContentPage):
    pass


class EnhancedResponseConclusionPage(TwoPortOnePathConclusionPage):
    pass


class EnhancedResponseWizard(TwoPortOnePathWizard):
    pass
