"""
    ===========================
    One Port Calibration Models
    ===========================

    This module defines the calibration models for One Port vector calibrations.

    See Also
    ----------
    sknrf.view.desktop.calibration.vector.oneport, sknrf.model.base.AbstractModel
"""
import abc
import logging
import os
import re
from itertools import combinations

import torch as th
import skrf
from skrf.calibration import OnePort, SDDL, PHN
from skrf.calibration import TwelveTerm, SOLT, EightTerm, UnknownThru, TRL
from skrf.calibration import TwoPortOnePath, EnhancedResponse

from sknrf.enums.runtime import SI, si_dtype_map
from sknrf.settings import Settings
from sknrf.model.calibration.base import AbstractCalibrationModel
from sknrf_transform_ff_system.model.system import CascadeTransform
from sknrf.enums.device import Instrument
from sknrf.utilities.numeric import AttributeInfo, Info
from sknrf.utilities.rf import t2a, n2t

logger = logging.getLogger(__name__)


class AbstractVectorModel(AbstractCalibrationModel):
    """A calibration model for vector calibrations.

        See Also
        ----------
        AbstractCalibrationModel
    """

    def __init__(self, instrument_flags=Instrument.RF):
        super(AbstractVectorModel, self).__init__(instrument_flags=instrument_flags)
        self.measurement_type = "SS"

    def __getstate__(self, state={}):
        state = super(AbstractVectorModel, self).__getstate__(state=state)
        # ### Manually save selected object PROPERTIES here ###
        state["measurement_type"] = self.measurement_type
        return state

    def __setstate__(self, state):
        super(AbstractVectorModel, self).__setstate__(state)
        # ### Manually load saved object ATTRIBUTES and PROPERTIES here ###
        self.measurement_type = state["measurement_type"]

    def __info__(self):
        super(AbstractVectorModel, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["measurement_type"] = Info("measurement type", read=False, write=False, check=False)

    def sort_ntwks(self, default=-1, reverse=False):
        """ Aligns two lists of networks based optional order and the intersection of their names.
        """
        measured = [measure for measure_name, measure in self.measured_ntwks.items()
                    for ideal_name, ideal in self.ideal_ntwks.items() if ideal_name in measure_name]
        ideals = [ideal for measure_name, measure in self.measured_ntwks.items()
                  for ideal_name, ideal in self.ideal_ntwks.items() if ideal_name in measure_name]
        measured = sorted(measured, key=lambda measure: self._order_index(measure, default=default), reverse=reverse)
        ideals = sorted(ideals, key=lambda ideal: self._order_index(ideal, default=default), reverse=reverse)
        return measured, ideals

    def _order_index(self, ntwk, default=-1):
        try:
            name = re.search(r"([\_]*[^\_]+)([\_\d]+)", ntwk.name).groups()[0]
            ntwk_order = [name.lower() for name in self.ntwk_order]
            return ntwk_order.index(name.lower())
        except AttributeError:
            return default
        except ValueError:
            return default

    def two_port_reflect(self):
        one_port_names = set([self.name_info(k)[0] for k, v in self.measured_ntwks.items() if v.nports == 1])
        two_port_names = set([k for k, v in self.measured_ntwks.items() if v.nports == 2])
        for name in one_port_names:
            for combo in combinations(self.port_indices, 2):
                new_name = "%s_%d_%d" % (name, combo[0], combo[1])
                reflect1 = self.ideal_ntwks.pop("_".join((name, str(combo[0]))))
                reflect2 = self.ideal_ntwks.pop("_".join((name, str(combo[1]))))
                if reflect1 and not reflect2:
                    reflect2 = reflect1
                if reflect2 and not reflect1:
                    reflect1 = reflect2
                if reflect1 and reflect2:
                    self.ideal_ntwks[new_name] = skrf.two_port_reflect(reflect1, reflect2)
                    self.ideal_ntwks[new_name].name = new_name
                else:
                    self.ideal_ntwks[new_name] = None
                reflect1 = self.measured_ntwks.pop("_".join((name, str(combo[0]))))
                reflect2 = self.measured_ntwks.pop("_".join((name, str(combo[1]))))
                self.measured_ntwks[new_name] = skrf.two_port_reflect(reflect1, reflect2)
                self.measured_ntwks[new_name].name = new_name
        for name in two_port_names:
            self.measured_ntwks.move_to_end(name)

    @abc.abstractmethod
    def calculate(self):
        return True

    @abc.abstractmethod
    def apply_cal(self):
        pass


class OnePortModel(AbstractVectorModel):
    """A calibration model for a One Port Calibration.

        See Also
        ----------
        OnePort
    """

    def calculate(self):
        """Calculates the error model coefficients.
        """
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = OnePort(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "1-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        port_num = self.port_indices[0]
        abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
        abcd[self.freq_slice()] = th.inverse(n2t(self.calibration.error_ntwk.a))
        transform = CascadeTransform(name, (port_num,), instrument_flags=self.instrument_flags,
                                     abcd=abcd)
        transforms.append(name, transform)


class SDDLModel(AbstractVectorModel):
    """A calibration model for a SDDL Calibration.

        See Also
        ----------
        SDDL
    """
    ntwk_order = ("Short", "Delay1", "Delay2", "Load")

    def calculate(self):
        """Calculates the error model coefficients.
        """
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = SDDL(ideals, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "1-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        port_num = self.port_indices[0]
        abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
        abcd[self.freq_slice()] = th.inverse(n2t(self.calibration.error_ntwk.a))
        transform = CascadeTransform(name, (port_num,), instrument_flags=self.instrument_flags,
                                     abcd=abcd)
        transforms.append(name, transform)


class PHNModel(AbstractVectorModel):
    """A calibration model for a PHN Calibration.

        See Also
        ----------
        PHN
    """

    def calculate(self):
        """Calculates the error model coefficients.
        """
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = PHN(ideals, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "1-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        port_num = self.port_indices[0]
        abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
        abcd[self.freq_slice()] = th.inverse(n2t(self.calibration.error_ntwk.a))
        transform = CascadeTransform(name, (port_num,), instrument_flags=self.instrument_flags,
                                     abcd=abcd)
        transforms.append(name, transform)


class TwelveTermModel(AbstractVectorModel):
    """A calibration model for a Twelve Term Calibration.

        See Also
        ----------
        TwelveTerm
    """

    def calculate(self):
        """Calculates the error model coefficients.
        """
        self.two_port_reflect()
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = TwelveTerm(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class SOLTModel(AbstractVectorModel):
    """A calibration model for a SOLT Calibration.

        See Also
        ----------
        SOLT
    """
    ntwk_order = ("Short", "Open", "Load", "Thru")

    def calculate(self):
        """Calculates the error model coefficients.
        """
        self.two_port_reflect()
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = SOLT(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class EightTermModel(AbstractVectorModel):
    """A calibration model for a EightTerm Calibration.

        See Also
        ----------
        EightTerm
    """

    def calculate(self):
        """Calculates the error model coefficients.
        """
        self.two_port_reflect()
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = EightTerm(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class UnknownThruModel(AbstractVectorModel):
    """A calibration model for a UnknownThru Calibration.

        See Also
        ----------
        UnknownThru
    """

    def calculate(self):
        """Calculates the error model coefficients.
        """
        self.two_port_reflect()
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = UnknownThru(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class TRLModel(AbstractVectorModel):
    """A calibration model for a TRL Calibration.

        See Also
        ----------
        TRL
    """
    ntwk_order = ("Thru", "Reflect", "Line")

    def calculate(self):
        """Calculates the error model coefficients.
        """
        self.two_port_reflect()
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = TRL(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class TwoPortOnePathModel(AbstractVectorModel):
    """A calibration model for a Two-Port One-Path Calibration.

        See Also
        ----------
        TwoPortOnePath
    """
    def __init__(self):
        super(TwoPortOnePathModel, self).__init__()
        self.source_port_index = 0

    def __getstate__(self, state={}):
        state = super(TwoPortOnePathModel, self).__getstate__(state=state)
        # ### Manually save selected object PROPERTIES here ###
        state["source_port_index"] = self.source_port_index
        return state

    def __setstate__(self, state):
        super(TwoPortOnePathModel, self).__setstate__(state)
        # ### Manually load saved object ATTRIBUTES and PROPERTIES here ###
        self.source_port_index = state["source_port_index"]

    def __info__(self):
        super(AbstractVectorModel, self).__info__()
        # ### Manually generate info of ATTRIBUTES and PROPERTIES here ###
        self.info["source_port_index"] = Info("source_port_index type", read=False, write=False, check=False)

    def calculate(self):
        """Calculates the error model coefficients.
        """
        page_names = ["Short", "Open", "Load"]
        combo = [self.source_port_index, self.port_indices[0]]
        thru = self.measured_ntwks["thru_" + "_".join([str(ind) for ind in combo])]
        for port_index in combo:
            for page_name in page_names:
                port_nums = "_%d" % (port_index,)
                ntwk = skrf.Network(os.sep.join((Settings().data_root, "calkits", "ideal", page_name.lower() + ".s1p")))
                self.ideal_ntwks[page_name + port_nums] = ntwk
                ntwk = ntwk.interpolate(thru.frequency)
                self.measured_ntwks[page_name + port_nums] = ntwk
        self.port_indices = combo
        self.two_port_reflect()
        self.port_indices = [combo[-1]]
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = TwoPortOnePath(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)


class EnhancedResponseModel(TwoPortOnePathModel):

    def calculate(self):
        """Calculates the error model coefficients.
        """
        page_names = ["Short", "Open", "Load"]
        combo = [self.source_port_index, self.port_indices[0]]
        thru = self.measured_ntwks["thru_" + "_".join([str(ind) for ind in combo])]
        for port_index in combo:
            for page_name in page_names:
                port_nums = "_%d" % (port_index,)
                ntwk = skrf.Network(os.sep.join((Settings().data_root, "calkits", "ideal", page_name.lower() + ".s1p")))
                self.ideal_ntwks[page_name + port_nums] = ntwk
                ntwk = ntwk.interpolate(thru.frequency)
                self.measured_ntwks[page_name + port_nums] = ntwk
        self.port_indices = combo
        self.two_port_reflect()
        self.port_indices = [combo[-1]]
        measured, ideals = self.sort_ntwks(default=len(self.measured_ntwks))
        self.calibration = EnhancedResponse(measured, ideals)
        self.calibration.run()

    def apply_cal(self):
        name = "2-port"
        t_points, f_points = Settings().t_points, Settings().f_points
        transforms = self.device_model().transforms
        t00 = self.calibration.error_ntwk[0].t[:, 0, 0].reshape(-1, 1, 1)
        k = self.calibration.coefs['k'].reshape(-1, 1, 1)
        for port_index, port_num in enumerate(self.port_indices):
            t = self.calibration.error_ntwk[port_index].t
            t = t/t00 if port_num == self.port_indices[0] else t/t00*k
            abcd = th.eye(2, dtype=si_dtype_map[SI.V]).repeat(t_points*f_points, 1, 1)
            abcd[self.freq_slice()] = th.inverse(t2a(n2t(t))[0])
            name_ = "%s_%d" % (name, port_index)
            transform = CascadeTransform(name_, (port_num,), instrument_flags=self.instrument_flags,
                                         abcd=abcd)
            transforms.append(name_, transform)
