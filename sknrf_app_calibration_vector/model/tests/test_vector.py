import unittest
import os

from sknrf.settings import Settings
from sknrf_app_calibration_vector.model.vector import OnePortModel, SDDLModel, PHNModel
from sknrf_app_calibration_vector.model.vector import TwelveTermModel, SOLTModel, EightTermModel, UnknownThruModel, TRLModel
from sknrf_app_calibration_vector.model.vector import TwoPortOnePathModel, EnhancedResponseModel
from sknrf.model.calibration.tests.test_calibration import CalibrationMethodTests


class TestOnePortCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = OnePortModel
    cal_port_indices = [1]
    cal_measurements = [["short", "_1", "short"],
                        ["open", "_1", "open"],
                        ["load", "_1", "load"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "OnePortModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestOnePortCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestOnePortCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestOnePortCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestOnePortCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestOnePortCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestOnePortCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestOnePortCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestOnePortCalibrationMethod, self).test_apply()


class TestTwelveTermCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = TwelveTermModel
    cal_port_indices = [1, 2]
    cal_measurements = [["short", "_1", "short"],
                        ["open", "_1", "open"],
                        ["load", "_1", "load"],
                        ["short", "_2", "short"],
                        ["open", "_2", "open"],
                        ["load", "_2", "load"],
                        ["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "TwelveTermModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestTwelveTermCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestTwelveTermCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestTwelveTermCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestTwelveTermCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestTwelveTermCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestTwelveTermCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestTwelveTermCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestTwelveTermCalibrationMethod, self).test_apply()


class TestSOLTCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = SOLTModel
    cal_port_indices = [1, 2]
    cal_measurements = [["short", "_1", "short"],
                        ["open", "_1", "open"],
                        ["load", "_1", "load"],
                        ["short", "_2", "short"],
                        ["open", "_2", "open"],
                        ["load", "_2", "load"],
                        ["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "SOLTModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestSOLTCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestSOLTCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestSOLTCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestSOLTCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestSOLTCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestSOLTCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestSOLTCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestSOLTCalibrationMethod, self).test_apply()


class TestEightTermCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = EightTermModel
    cal_port_indices = [1, 2]
    cal_measurements = [["short", "_1", "short"],
                        ["open", "_1", "open"],
                        ["load", "_1", "load"],
                        ["short", "_2", "short"],
                        ["open", "_2", "open"],
                        ["load", "_2", "load"],
                        ["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "EightTermModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestEightTermCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestEightTermCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestEightTermCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestEightTermCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestEightTermCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestEightTermCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestEightTermCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestEightTermCalibrationMethod, self).test_apply()


class TestUnknownThruCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = UnknownThruModel
    cal_port_indices = [1, 2]
    cal_measurements = [["short", "_1", "short"],
                        ["open", "_1", "open"],
                        ["load", "_1", "load"],
                        ["short", "_2", "short"],
                        ["open", "_2", "open"],
                        ["load", "_2", "load"],
                        ["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "UnknownThruModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestUnknownThruCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestUnknownThruCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestUnknownThruCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestUnknownThruCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestUnknownThruCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestUnknownThruCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestUnknownThruCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestUnknownThruCalibrationMethod, self).test_apply()


class TestTRLCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = TRLModel
    cal_port_indices = [1, 2]
    cal_measurements = [["thru", "_1_2", "thru"],
                        ["reflect", "_1_2", "thru"],
                        ["line", "_1_2", "line"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "TRLModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def test_measurement(self):
        super(TestTRLCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestTRLCalibrationMethod, self).test_save_measurement()

    @unittest.expectedFailure
    def test_calculate(self):
        super(TestTRLCalibrationMethod, self).test_calculate()

    @unittest.expectedFailure
    def test_calculate_lf(self):
        super(TestTRLCalibrationMethod, self).test_calculate_lf()

    @unittest.expectedFailure
    def test_calculate_rf(self):
        super(TestTRLCalibrationMethod, self).test_calculate_rf()

    @unittest.expectedFailure
    def test_save(self):
        super(TestTRLCalibrationMethod, self).test_save()

    @unittest.expectedFailure
    def test_load(self):
        super(TestTRLCalibrationMethod, self).test_load()

    @unittest.expectedFailure
    def test_apply(self):
        super(TestTRLCalibrationMethod, self).test_apply()


class TestTwoPortOnePathCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = TwoPortOnePathModel
    cal_port_indices = [2]
    cal_measurements = [["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "TwoPortOnePathModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def setUp(self):
        CalibrationMethodTests.setUp(self)
        self.cal_model.source_port_index = 1

    def test_measurement(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestTwoPortOnePathCalibrationMethod, self).test_apply()


class TestEnhancedResponseCalibrationMethod(CalibrationMethodTests, unittest.TestCase):

    cal_class = EnhancedResponseModel
    cal_port_indices = [2]
    cal_measurements = [["thru", "_1_2", "thru"]]
    cal_filename = os.sep.join((Settings().data_root, "saved_calibrations", "testdata.cal"))
    calkit_dg = "EnhancedResponseModel"
    calkit_dg_dir = os.sep.join((Settings().data_root, "datagroups"))
    calkit_dir1 = os.sep.join((Settings().data_root, "calkits", "ideal"))
    calkit_dir2 = os.sep.join((Settings().data_root, "calkits", "ideal_ideal"))
    caldata_dir = os.sep.join((Settings().data_root, "caldata"))

    def setUp(self):
        CalibrationMethodTests.setUp(self)
        self.cal_model.source_port_index = 1

    def test_measurement(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_measurement()

    def test_save_measurement(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_save_measurement()

    def test_calculate(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_calculate()

    def test_calculate_lf(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_calculate_lf()

    def test_calculate_rf(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_calculate_rf()

    def test_save(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_save()

    def test_load(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_load()

    def test_apply(self):
        super(TestEnhancedResponseCalibrationMethod, self).test_apply()


def vector_calibration_test_suite():
    test_suite = unittest.TestSuite()

    test_suite.addTest(unittest.makeSuite(TestOnePortCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestTwelveTermCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestSOLTCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestEightTermCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestUnknownThruCalibrationMethod))
    test_suite.addTest(unittest.makeSuite(TestTRLCalibrationMethod))

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(vector_calibration_test_suite())

